const puppeteer = require('puppeteer');
let config ={
    launchOptions: {
        headless:true
    }
}
puppeteer.launch(config.launchOptions).then(async browser => {
    const page = await browser.newPage();
    await page.goto('https://app.hubspot.com/login');
    await browser.close();
});